Git--> way to manage **code**
- keeps track of every revision
- version control software
**IMPORTANT Words**-->
1. Repository(repo)- collection of files and folders(code files) using git to track
2. GitLab-re,ote storage solution for got Repository
3. Commit- similar to saving the work in the local machine
4. Push- syncing commits to GitLab
5. Brach- similar kind of structure to tree. Bark of tree i.e main software  is known as **Master branch**
**branch**- separate instances of code.and  different from main codebase.
6. Merge- Integrates two  branches together.
> branch free of bugs merge into Master branch.
7. Clone-entire online Repository and makes exact duplicate copy of it on local machine.
8. Fork- Similar to cloning
-makes entirely new repo of that code under users name
- does not make duplicate copy 

**Git Internals**
3 types:
1. Modified- changed the file but have not committed to repo yet
2. staged- Marked as modified file in its current version to go into next picture
3. committed- data is safely stored in local repo in form of pictures

                 >   **Oncomputer**
                 - Workspace- all changes done via editor done in this tree(git add--.>)
                 - Staging- all staged files go into this tree(git commit-->)
                 - Local repo- all committed files go to this tree(git push-->)
                >    **on Internet**
                - Remote repo- copy of local repo but stored on some server on Internet
                  - changes must be pushed.
